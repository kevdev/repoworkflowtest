package de.szut.dqi12neumeyer;

public class Main {
	public static void main(String[] args) throws SudokuException {
		Sudoku s = new Sudoku();
		int[][] solvedBoard = null;
		solvedBoard = s.generateSolvedBoard();
		System.out.println(s.printBoard(solvedBoard));

	}
}
