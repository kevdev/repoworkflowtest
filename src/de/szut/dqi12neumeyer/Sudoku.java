package de.szut.dqi12neumeyer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author hh
 * 
 */
public class Sudoku implements SudokuInterface {

	/* D1000: a 9x9 valid sudoku board */
	int[][] solvedboard = null;

	/* D1001: a 9x9 gameboard */
	int[][] gameboard = null;

	/* D1002: board for playing */
	public int[][] userboard = null;

	/* D1003: list with int 1 to 9 */
	ArrayList<Integer> allList = new ArrayList<Integer>();

	/* D1004: list which contains possible Integers left */
	ArrayList<Integer> possibleList = new ArrayList<Integer>();

	@Override
	public int[][] generateSolvedBoard() throws SudokuException {
		// Die letzte Zahl die gesetzt wurde
		int lastNum = 0;

		// Die Anzahl der gescheiterten Versuche
		int attempts = 0;

		// Die aktuelle Zahl die gesetzt wird
		int num = 0;

		// Flag das angibt ob ein Schritt zur�ck gemacht wurde (Damit nicht
		// dieselbe Zahl noch einmal probiert wird)
		boolean backtrack = false;

		for (int y = 0; y < 9; y++) {
			for (int x = 0; x < 9; x++) {
				possibleList = removeNeighbors(removeAboves(removeLefts((ArrayList<Integer>)allList.clone(), y, x), y, x), y, x);

				if (possibleList.size() == 0) {
					// Einen Schritt zur�ck gehen, Backtracken wei�scht
					if (x == 0) {
						y--;
						x = 8;
					} else {
						x--;
					}

					// Anzahl der gescheiterten Versuche erh�hen z�hlen
					attempts++;

					// Es muss ein Schritt zur�ck gemacht werden
					backtrack = true;

					// Bei 3 gescheiterten versuchen abbrechen
					if (attempts == 3) {
						this.printBoard(solvedboard);
						return solvedboard;
//						throw new SudokuException("Geeht niiiiiiiiiiiiiiiiiiicht");
					}
				} else {
					if (backtrack) {
						do {
							num = possibleList.get((int) Math.random() * 9);
						} while (num == lastNum);
						
						backtrack = false;
					} else {
						num = possibleList.get((int)(Math.random() * possibleList.size()));
					}

					lastNum = num;
				}
				
				//Wert ins board setzen
				this.solvedboard[y][x] = num;
			}
		}

		return this.solvedboard;
	}

	public Sudoku() {
		// Arrays f�r solvedboard, gameboard und userboard erzeugen
		this.solvedboard = new int[9][9];
		this.gameboard = new int[9][9];
		this.userboard = new int[9][9];

		// Liste mit allen m�glichen Zahlen auff�llen
		for (int num = 1; num <= 9; num++) {
			allList.add(num);
		}
	}

	@Override
	public int[][] generateGameBoard(Level level) {
		int[][] gameboard = new int[9][9];
		
		for(int y = 0; y<9; y++){
			for(int x = 0; x<9; x++){
				gameboard[y][x] = solvedboard[y][x];
			}
		}
			
		//Gibt an wieviele Zahlen entfernt werden sollen
		int removeNums = 0;
		
		switch (level) {
		case HEAVY:
			removeNums = 15;
		case EASY:
			removeNums = 5;
		case DIFFICULT:
			removeNums = 20;
		case MEDIUM:
			removeNums = 10;
		case EXTREME_HARDCORE_INSANE:
			removeNums = 40;
		}
		
		int x,y;
		
		for(int i = 0; i  < removeNums; i++){
			x = (int) Math.random() * 9;
			y = (int) Math.random() * 9;
			
			gameboard[y][x] = 0;
		}
		
		return gameboard;
	}

	@Override
	public int[][] generateUserBoard(Level level) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String printBoard(int[][] board) {
		StringBuilder output = new StringBuilder();

		output.append("+---+---+---+---+---+---+---+---+---+\n+");
		
		for (int[] line : board) {
			for (int number : line) {
				output.append(" ").append(number).append(" ").append("+");
			}

			output.append("\n+---+---+---+---+---+---+---+---+---+\n+");
		}

		return output.toString();
	}

	@Override
	public ArrayList<Integer> removeLefts(ArrayList<Integer> line, int row,
			int col) {
		for (int x = col; x >= 0; x--) {
			line.remove((Object) this.solvedboard[row][x]);
		}

		return line;
	}

	@Override
	public ArrayList<Integer> removeAboves(ArrayList<Integer> line, int row,
			int col) {
		for (int y = row; y >= 0; y--) {
			line.remove((Object) this.solvedboard[y][col]);
		}

		return line;
	}

	@Override
	public ArrayList<Integer> removeNeighbors(ArrayList<Integer> line, int row,
			int col) {
//		System.out.println("Board:" + this.printBoard(solvedboard));
//		System.out.println("Zeile:" + Integer.toString(row));
//		System.out.println("Spalte:" +Integer.toString(col));
//		System.out.println("Line before:"+ line.toString());
		
		for (int y = row / 3; y < (row / 3) + 3; y++) {
			for (int x = col / 3; x < (col / 3) + 3; x++) {
				line.remove((Object) this.solvedboard[y][x]);
//				System.out.println("Checke: "+Integer.toString(x) + ", " + Integer.toString(y));
			}
		}

//		System.out.println("Line after:" + line.toString()); 
		
		return line;
	}
}
