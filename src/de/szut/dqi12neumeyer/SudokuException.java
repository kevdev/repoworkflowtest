package de.szut.dqi12neumeyer;
/*
 */
public class SudokuException extends Exception {
	String message;

	/**
	 * Der Konstruktor der Klasse SudokuException wird definiert. Dieser nimmt
	 * einen Parameter auf.
	 * 
	 * @param message
	 *            vom Typ String wird aufgenommen und der Klassenvariable
	 *            message �bergeben.
	 */
	public SudokuException(String message) {
		this.message = message;
	}

}

