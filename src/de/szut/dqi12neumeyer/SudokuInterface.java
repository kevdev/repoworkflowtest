package de.szut.dqi12neumeyer;
import java.util.ArrayList;

/**
 * 
 */
public interface SudokuInterface {
	
	
	/* F1000: generateSolvedBoard() builds valid sudoku game board
	 * 1.) 9x9 game board filled with 0.<br />
	 * 2.) Generate a List with 1 to 9.<br />
	 * 3. Each field is set to a number. Number is extracted by using:
	 *    removeLefts(), removeAboves(), removeNeighbors(). 
	 *    Possible number ist set to focused field. 
	 * 3.a) Check rule violations of Sudoku<br />
	 * 3.b) Too many trials to find a valid number for a field? 
	 *      3 times of locking = begin with 1.)<br />
	 * 4.) The gameboard is created and sets 'success' to true. <br/>
	 * 5.) Adjust game level resetting serveral fields to null.
	 * 6.) User may start playing. 
	 * 7.) @throws SudokuException when failing
	 */
	public int[][] generateSolvedBoard() throws SudokuException;

	
	/* F1001: generates a board by using a level - kills elements by using level
	 */
	public int[][] generateGameBoard(Level level);

	
	
	/* F1002: generates a userBoard by copying a game board.
	 * user plays in user board. if user want's to restart the game
	 * recover it by using gameboard.
	 */
	public int[][] generateUserBoard(Level level);

			
	/**
	 * F1003: printBoard()
	 * 
	 * returns formatted Board
	 */
	public String printBoard(int[][] board);


	/**
	 * F1004: removeLefts(ArrayList<Integer> line, int row, int col)
	 * 
	 * @return ArrayList<Integer>
	 * @param ArrayList
	 *            <Integer>, int row, int col
	 */
	public ArrayList<Integer> removeLefts(ArrayList<Integer> line, int row,
			int col);

	/**
	 * F1005: removeAboves(ArrayList<Integer> line, int row, int col)
	 * 
	 * @return ArrayList<Integer>
	 * @param ArrayList
	 *            <Integer>, int row, int col Remove the numbers which are above
	 *            the number at (row,col)
	 */
	public ArrayList<Integer> removeAboves(ArrayList<Integer> line, int row,
			int col);

	/**
	 * F1006: removeNeighbors(ArrayList<Integer> line, int row, int col)
	 * 
	 * @return: ArrayList<Integer> line
	 * @param ArrayList
	 *            <Integer>, int row, int col
	 */
	public ArrayList<Integer> removeNeighbors(ArrayList<Integer> line, int row,
			int col);

}

